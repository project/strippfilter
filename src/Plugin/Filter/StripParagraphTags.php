<?php

namespace Drupal\strippfilter\Plugin\Filter;

use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;

/**
 * Provides a filter to strip HTML tags.
 *
 * The attributes in the annotation shows example how to strip script tag(s)
 * from display
 *
 * @Filter(
 *   id = "strippfilter",
 *   title = @Translation("Strip paragraph tags"),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_REVERSIBLE,
 *   weight = -10
 * )
 */
class StripParagraphTags extends FilterBase {

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode) {
    return new FilterProcessResult(strippfilter_process($text));
  }
}
